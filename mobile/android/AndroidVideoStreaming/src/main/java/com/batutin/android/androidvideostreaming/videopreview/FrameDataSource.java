package com.batutin.android.androidvideostreaming.videopreview;

/**
 * Created by Andrew Batutin on 8/27/13.
 */
public interface FrameDataSource {

    public byte [] createVideoFrame();
}
