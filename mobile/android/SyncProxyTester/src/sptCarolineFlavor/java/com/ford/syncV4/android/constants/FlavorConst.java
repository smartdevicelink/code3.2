package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SPTCaroline";
    public static final String APPID_BT = "11675309";
    public static final String APPID_TCP = "11675308";
    public static final String APPID_USB = "11675310";
}
