package com.ford.syncV4.android.listener;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 1/27/14
 * Time: 10:44 AM
 */
public interface ConnectionListener {
    void onProxyClosed();
}