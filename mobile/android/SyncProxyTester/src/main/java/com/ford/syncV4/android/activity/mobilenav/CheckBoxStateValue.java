package com.ford.syncV4.android.activity.mobilenav;

/**
 * Created by Andrew Batutin on 8/30/13.
 */
public enum CheckBoxStateValue {

    ON, OFF, DISABLED

}
