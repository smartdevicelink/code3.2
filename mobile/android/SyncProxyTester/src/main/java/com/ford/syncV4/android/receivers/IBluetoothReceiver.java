package com.ford.syncV4.android.receivers;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/26/13
 * Time: 5:39 PM
 */
public interface IBluetoothReceiver {
    void onBluetoothOn();
    void onBluetoothOff();
    void onBluetoothTurningOff();
}