package com.ford.syncV4.android.manager;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/26/13
 * Time: 6:00 PM
 */
public interface IBluetoothDeviceManager {
    void onBluetoothDeviceRestoreConnection();
    void onBluetoothDeviceTurningOff();
}