package com.ford.syncV4.android.service;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 1/29/14
 * Time: 5:02 PM
 */
public interface IProxyServiceBinder {
    void onServiceBindComplete();
}