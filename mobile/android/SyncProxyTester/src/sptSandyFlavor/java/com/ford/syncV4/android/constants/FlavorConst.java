package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SPTSandy";
    public static final String APPID_BT = "13675309";
    public static final String APPID_TCP = "13675308";
    public static final String APPID_USB = "13675310";
}
