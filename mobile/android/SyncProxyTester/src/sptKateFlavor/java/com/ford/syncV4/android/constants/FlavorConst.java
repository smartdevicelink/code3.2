package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SPTKate";
    public static final String APPID_BT = "12675309";
    public static final String APPID_TCP = "12675308";
    public static final String APPID_USB = "12675310";
}
