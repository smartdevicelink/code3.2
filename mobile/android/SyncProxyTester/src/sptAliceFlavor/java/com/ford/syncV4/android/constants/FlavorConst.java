package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SPTAlice";
    public static final String APPID_BT = "9675309";
    public static final String APPID_TCP = "9675308";
    public static final String APPID_USB = "9675310";
}
