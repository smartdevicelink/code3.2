package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SPTAmy";
    public static final String APPID_BT = "10675309";
    public static final String APPID_TCP = "10675308";
    public static final String APPID_USB = "10675310";
}
