package com.ford.syncV4.android.constants;

public class FlavorConst {
    public static final String PREFS_DEFAULT_APPNAME = "SyncProxyTester";
    public static final String APPID_BT = "8675309";
    public static final String APPID_TCP = "8675308";
    public static final String APPID_USB = "8675310";
}
