package com.ford.syncV4.protocol.enums;

public enum MessageType {
//	START_SESSION,
//	START_SESSION_ACK,
//	START_SESSION_NACK,
//	END_SESSION,
	UNDEFINED,
	BULK,
	RPC,
    VIDEO
}
