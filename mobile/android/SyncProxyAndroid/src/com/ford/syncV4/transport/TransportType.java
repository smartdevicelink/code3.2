package com.ford.syncV4.transport;

/**
 * Defines available types of the transports.
 */
public enum TransportType {
	
	/**
	 * Transport type is Bluetooth.
	 */
	BLUETOOTH,
	
	/**
	 * Transport type is TCP.
	 */
	TCP,

    /**
     * Transport type is USB.
     */
    USB
}
