package com.ford.syncV4.exception;

public enum SyncExceptionCause {
	BLUETOOTH_ADAPTER_NULL,
	BLUETOOTH_DISABLED,
	HEARTBEAT_PAST_DUE,
	INCORRECT_LIFECYCLE_MODEL,
	INVALID_ARGUMENT,
	INVALID_RPC_PARAMETER,
	PERMISSION_DENIED,
	RESERVED_CORRELATION_ID,
	SYNC_CONNECTION_FAILED,
	SYNC_CONNECTION_INIT_EXCEPTION,
	SYNC_PROXY_CYCLED,
	SYNC_PROXY_DISPOSED,
	SYNC_PROXY_OBSOLETE,
	SYNC_REGISTRATION_ERROR,
	SYNC_UNAVAILALBE,
	SYNC_USB_DETACHED,
	SYNC_USB_PERMISSION_DENIED
}