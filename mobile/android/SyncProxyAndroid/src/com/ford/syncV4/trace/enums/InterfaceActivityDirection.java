package com.ford.syncV4.trace.enums;

public enum InterfaceActivityDirection {
	Transmit,
	Receive,
	None;
}
