package com.ford.syncV4.trace;

public interface ISTListener {
	void logXmlMsg(String msg, String token);
} // end-interface