package com.ford.syncV4.proxy.callbacks;

import com.ford.syncV4.proxy.constants.Names;

public class OnProxyOpened extends InternalProxyMessage {

	public OnProxyOpened() {
		super(Names.OnProxyOpened);
	}
}
