package com.ford.syncV4.proxy.rpc.enums;

public enum SyncConnectionState {
	SYNC_CONNECTED,
	SYNC_DISCONNECTED;
}
