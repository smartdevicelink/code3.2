package com.ford.syncV4.proxy.interfaces;

import com.ford.syncV4.proxy.rpc.OnSyncPData;

public interface ISyncSyncPDataListener {
	public void onOnSyncPData(OnSyncPData notification);
}