package com.ford.syncV4.proxy.interfaces;

import com.ford.syncV4.proxy.rpc.DeleteInteractionChoiceSetResponse;

public interface ISyncDeleteInteractionChoiceSetResponseListener {
	public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSetResponse response, Object tag);
}