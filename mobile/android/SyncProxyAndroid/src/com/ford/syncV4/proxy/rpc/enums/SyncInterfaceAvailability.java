package com.ford.syncV4.proxy.rpc.enums;

public enum SyncInterfaceAvailability {
	SYNC_INTERFACE_AVAILABLE,
	SYNC_INTERFACE_UNAVAILABLE;
}
