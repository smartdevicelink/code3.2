package com.ford.syncV4.proxy.interfaces;

import com.ford.syncV4.proxy.rpc.OnDriverDistraction;

public interface ISyncDriverDistractionListener {
	public void onOnDriverDistraction(OnDriverDistraction notification);
}