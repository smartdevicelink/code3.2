package com.ford.syncV4.proxy.interfaces;

import com.ford.syncV4.proxy.rpc.OnEncodedSyncPData;

public interface ISyncEncodedSyncPDataListener {
	public void onOnEncodedSyncPData(OnEncodedSyncPData notification);
}